#!/usr/bin/env python3
import os
os.chdir(os.path.dirname(__file__))  # For å finne inputfila
with open('1-input.txt') as f:
    lines = f.readlines()

sum = 0
for line in lines:
    sum += int(line)

print("Answer 1: {}".format(sum))

sum = 0
freqs = set()
found = False
while found is False:  # Need to go through all multiple times
    for line in lines:
        sum += int(line)
        if sum in freqs:
            found = True
            break
        freqs.add(sum)
print("Answer 2: {}".format(sum))
