#!/usr/bin/env python3
import os
os.chdir(os.path.dirname(__file__))  # For å finne inputfila


class Coord:
    def __init__(self, x, y):
        self.x = int(x)
        self.y = int(y)
        self.area = 0
        self.infinite_area = False


def read_file():
    with open("6-input.txt", "r") as myfile:
        lines = [line.rstrip('\n\r') for line in myfile]
    return lines


def populate_coords(lines):
    """ Returns list of Coord-objects """
    coords = []
    for line in lines:
        x = int(line.split(",")[0])
        y = int(line.split(",")[1])
        print(x, y)
        coords.append(Coord(x, y))
    return coords


def distance(x1, y1, x2, y2):
    return abs(x1 - x2) + abs(y1 - y2)


def main():
    lines = read_file()
    coords = populate_coords(lines)
    minx = min(coords, key=lambda a: a.x).x
    maxx = max(coords, key=lambda a: a.x).x + 1
    miny = min(coords, key=lambda a: a.y).y
    maxy = max(coords, key=lambda a: a.y).y + 1
    print("Borders:", minx, miny, maxx, maxy)

    safe_region_size = 0  # For part 2
    for y in range(miny, maxy):
        for x in range(minx, maxx):
            min_dist = None
            has_equal = False
            sum_dist = 0
            for coord in coords:
                dist = distance(x, y, coord.x, coord.y)
                sum_dist += dist
                if min_dist is None or dist < min_dist:
                    has_equal = False
                    min_dist = dist
                    nearest_coord = coord
                elif dist == min_dist:
                    has_equal = True
            if not has_equal:
                nearest_coord.area += 1
            if x in [minx, maxx] or y in [miny, maxy]:
                nearest_coord.infinite_area = True
            if sum_dist < 10000:  # Part 2
                safe_region_size += 1

    for coord in coords:
        print(coord.x, coord.y, coord.area, coord.infinite_area)

    max_value = max(filter(lambda x: x.infinite_area is False, coords), key=lambda x: x.area).area
    print("Answer 1: {}".format(max_value))
    print("Answer 2: {}".format(safe_region_size))


if __name__ == "__main__":
    main()
