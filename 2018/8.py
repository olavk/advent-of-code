#!/usr/bin/env python3
import os
os.chdir(os.path.dirname(__file__))  # For å finne inputfila


def read_file():
    with open("8-input.txt", "r") as myfile:
        lines = [line.rstrip('\n\r') for line in myfile]
    return lines


def do_node(numbers):
    sum_from_children = 0
    sum_metadata = 0
    num_child_nodes = int(numbers.pop(0))
    num_metadata = int(numbers.pop(0))
    # print("children: {}, metadata_entries: {}".format(num_child_nodes, num_metadata))
    for c in range(num_child_nodes):
        sum_from_children += do_node(numbers)
    for m in range(num_metadata):
        sum_metadata += int(numbers.pop(0))
    return sum_metadata + sum_from_children


def do_node2(my_id, numbers):
    values_from_children = []
    metadata_value = 0
    value = 0
    num_child_nodes = int(numbers.pop(0))
    num_metadata = int(numbers.pop(0))
    print("node {}'s children: {}, metadata_entries: {}".format(my_id, num_child_nodes, num_metadata))
    for c in range(num_child_nodes):
        print("child node: {}".format(c))
        values_from_children.append(do_node2(my_id + 1, numbers))
        print("values from node {}'s children after {}: {}".format(my_id, c, values_from_children))
    if num_child_nodes == 0:
        for m in range(num_metadata):
            metadata_value += int(numbers.pop(0))
        value = metadata_value
    else:
        for m in range(num_metadata):
            metadata_value = int(numbers.pop(0))
            print("node {}'s metadata_value: {}, child nodes:{}".format(my_id, metadata_value, num_child_nodes))
            if metadata_value != 0 and metadata_value <= num_child_nodes:
                value += values_from_children[metadata_value - 1]

    print("returnerer {}".format(value))
    return value


def main():
    lines = read_file()
    numbers1 = lines[0].split(' ')
    numbers2 = lines[0].split(' ')
    # numbers2 = [2, 3, 0, 3, 10, 11, 12, 1, 1, 0, 1, 99, 2, 1, 1, 2,]
    sum = do_node(numbers1)
    sum2 = do_node2(1, numbers2)

    print("Answer 1: {}".format(sum))
    print("Answer 2: {}".format(sum2))


if __name__ == "__main__":
    main()
