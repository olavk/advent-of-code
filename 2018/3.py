#!/usr/bin/env python3
import os
os.chdir(os.path.dirname(__file__))  # For å finne inputfila

# #1 @ 1,3: 4x4
with open('3-input.txt') as f:
    lines = [line.rstrip('\n') for line in open('3-input.txt')]

area = dict()


def claim_coord(x, y):
    # print("Claim:", x, y)
    if y not in area:
        area[y] = dict()
    if x not in area[y]:
        area[y][x] = 1
        return False
    else:
        area[y][x] += 1
        # print("Overlap:", x, y)
        return True


for claim in lines:
    """ Fill up all claims, increasing number for each overlap """
    x = int(claim.split()[2].split(",")[0])
    y = int(claim.split()[2].split(",")[1].split(":")[0])
    sizex = int(claim.split()[3].split("x")[0])
    sizey = int(claim.split()[3].split("x")[1])
    # print(x, y, sizex, sizey)
    for row in range(y, y + sizey):
        for column in range(x, x + sizex):
            claim_coord(column, row)


overlaps = 0
for row in area:
    for column in area[row]:
        if area[row][column] > 1:
            overlaps += 1

print("Answer 1: {}".format(overlaps))

for claim in lines:
    has_overlap = False
    claimno = int(claim.split()[0].split("#")[1])
    x = int(claim.split()[2].split(",")[0])
    y = int(claim.split()[2].split(",")[1].split(":")[0])
    sizex = int(claim.split()[3].split("x")[0])
    sizey = int(claim.split()[3].split("x")[1])
    for row in range(y, y + sizey):
        for column in range(x, x + sizex):
            if area[row][column] > 1:
                has_overlap = True
    if not has_overlap:
        print("Claim {0:d} has no overlap".format(claimno))
        print("Answer 2: {0:d}".format(claimno))
