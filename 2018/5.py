#!/usr/bin/env python3
import os
os.chdir(os.path.dirname(__file__))  # For å finne inputfila

# ('Units left after removing', 'x', '/', 'X', 5278)


def remove_dups(string):
    # print(string)
    new_string = []
    for charindex in range(len(string)):
        if len(new_string) < 1:
            new_string.append(string[charindex])
            continue
        new_string.append(string[charindex])
        # print("comparing", new_string[-2], new_string[-1], new_string)
        if is_destroyed(new_string[-2], new_string[-1]):
            # print("popping", new_string[-2], new_string[-1])
            new_string.pop()
            new_string.pop()
    # print(new_string)
    return "".join(new_string)


def is_destroyed(s1, s2):
    if s1.islower() and s2.isupper() and s1.upper() == s2:
        return True
    if s1.isupper() and s2.islower() and s1.lower() == s2:
        return True
    return False


def main():
    with open("5-input.txt", "r") as myfile:
        chars = myfile.read()
    # chars = "JGgjCcpPeu"
    while True:
        new_chars = remove_dups(chars)
        print("new_chars:", new_chars)
        if new_chars == chars:
            break
        chars = new_chars

    print("Reduced string: {}".format(chars))
    print("Answer 1: {}".format(len(chars)))

    min_length = None
    for remove_char in list(map(chr, range(ord('a'), ord('z')+1))):
        string = chars.replace(remove_char, "")
        string = string.replace(remove_char.upper(), "")
        string = remove_dups(string)
        # print("Reduced string:", string)
        print("Units left after removing {}/{}: {}".format(remove_char, remove_char.upper(), len(string)))
        min_length = min(len(string), min_length) if min_length is not None else len(string)

    print("Answer 2: {}".format(min_length))


if __name__ == "__main__":
    main()
