#!/usr/bin/env python3
import os
os.chdir(os.path.dirname(__file__))  # For å finne inputfila


def compare_strings(s1, s2):
    """ Returns number of characters that differ """
    diff = 0
    for x in range(len(s1)):
        if not s1[x] == s2[x]:
            diff += 1
    return diff


def print_common_chars(s1, s2):
    """ Returns string with all characters that are common between 2 strings """
    s = ""
    for x in range(len(s1)):
        if s1[x] == s2[x]:
            s = s + s1[x]
    return s


with open('2-input.txt') as f:
    lines = f.readlines()
    # lines = [line.rstrip('\n') for line in open('filename')]

no_lines_with_twos = int()
no_lines_with_threes = int()

for line in lines:
    charfreq = dict()
    has_twos = False
    has_threes = False
    for c in line:
        if c in charfreq:
            charfreq[c] += 1
        else:
            charfreq[c] = 1
    for c in charfreq:
        if charfreq[c] == 2:
            has_twos = True
        elif charfreq[c] == 3:
            has_threes = True
    if has_twos:
        no_lines_with_twos += 1
    if has_threes:
        no_lines_with_threes += 1

print("Answer 1: {}".format(no_lines_with_twos * no_lines_with_threes))

lineno = 0
for curline in lines:
    lineno += 1
    for compareline in lines[lineno+1:]:
        diff = compare_strings(curline, compareline)
        if diff < 2:
            print(curline)
            print(compareline)
            print("Num diffs: {}".format(diff))
            print("Answer 2: {}".format(print_common_chars(curline, compareline)))
            break  # Stops after first found with only 1 diff
