#!/usr/bin/env python3
import os
os.chdir(os.path.dirname(__file__))  # For å finne inputfila

# Step T must be finished before step C can begin.
# BITRAQVSGUWKXYHMZPOCDLJNFE


def read_file():
    with open("7-input.txt", "r") as myfile:
        lines = [line.rstrip('\n\r') for line in myfile]
    return lines


def get_all_steps(lines):  # From both steps and prerequisites
    all_steps = {}  # 'step': ['a', 'b']
    for line in lines:
        step = line.split()[7]
        prerequisite = line.split()[1]
        if step in all_steps:
            prerequisites = all_steps[step]
            prerequisites.append(prerequisite)
        else:
            prerequisites = [prerequisite]
        if prerequisite not in all_steps:
            all_steps[prerequisite] = []
        all_steps[step] = sorted(prerequisites)
    return all_steps


def get_next_step(steps):
    empty_steps = []
    for step in steps:
        if len(steps[step]) == 0:
            empty_steps.append(step)
    if len(empty_steps) == 0:
        return False
    else:
        return sorted(empty_steps)[0]


def remove_prerequisite(steps, prerequisite):
    for step in steps:
        if prerequisite in steps[step]:
            steps[step].remove(prerequisite)


def main():
    lines = read_file()
    all_steps = get_all_steps(lines)
    print(all_steps)

    steps_in_order = []
    while True:
        next_step = get_next_step(all_steps)
        if next_step is False:
            break
        steps_in_order.append(next_step)
        print(f'order: {steps_in_order}')

        del all_steps[next_step]
        remove_prerequisite(all_steps, next_step)
        print("all steps", all_steps)

    print(f'Answer 1: {"".join(steps_in_order)}')


if __name__ == "__main__":
    main()
