#!/usr/bin/env python3
import os
os.chdir(os.path.dirname(__file__))  # For å finne inputfila

# [1518-10-24 00:52] falls asleep
# [1518-05-26 23:59] Guard #71 begins shift
# [1518-09-04 23:57] Guard #3299 begins shift
# [1518-06-01 00:50] wakes up


def get_time(line):
    hour = int(line.split()[1].split(":")[0])
    if hour == 23:
        return 0
    else:
        return int(line.split()[1].split(":")[1].strip("]"))


def get_guard_with_most_minutes(guards):
    """ Returns tuple with guard_id and minutes """
    guard_id = 0
    max_no_minutes = 0
    for g, m in guards.items():
        minutes = sum(m)
        if minutes > max_no_minutes:
            max_no_minutes = minutes
            guard_id = g
    return (guard_id, max_no_minutes)


def get_guard_with_most_single_minute(guards):
    """ Returns tuple with guard_id, sleepiest minute, and number of minutes slept """
    guard_id = 0
    max_sleepiest_minute = 0
    max_num_minutes = 0
    for g, m in guards.items():
        sleepiest_minute, no_minutes = get_sleepiest_minute(guards, g)
        if no_minutes > max_num_minutes:
            guard_id = g
            max_sleepiest_minute = sleepiest_minute
            max_num_minutes = no_minutes
    return (guard_id, max_sleepiest_minute, max_num_minutes)


def get_sleepiest_minute(guards, guard_id):
    """ The minute of the hour this guard slept longest, and the number of times he slept """
    sleepiest_minute = 0
    max_num_minutes = 0
    for minute in range(60):
        if guards[guard_id][minute] > max_num_minutes:
            max_num_minutes = guards[guard_id][minute]
            sleepiest_minute = minute
    return (sleepiest_minute, max_num_minutes)


def main():
    with open('4-input.txt') as f:
        lines = [line.rstrip('\n\r') for line in f]

    lines.sort()
    guards = dict()
    time = 0

    # Build up dict of arrays with number of minutes asleep per minute
    # guards[guard][0..60] = minutes asleep
    for line in lines:
        print(line)
        prev_time = time
        time = get_time(line)
        if line.find("Guard") != -1:
            guard = int(line.split()[3].split("#")[1])
            if guard not in guards:
                guards[guard] = [0] * 60
        elif line.find("asleep") != -1:
            pass
        elif line.find("wakes") != -1:
            for x in range(prev_time, time):
                guards[guard][x] += 1

    print("All guards, minutes asleep")
    print(guards)

    guard_id, minutes = get_guard_with_most_minutes(guards)
    print("Guard, minutes:", guard_id, minutes)

    sleepiest_minute, num_minutes = get_sleepiest_minute(guards, guard_id)
    print("sleepiest minute:", sleepiest_minute)

    print("Guard times chosen minute: ", guard_id * sleepiest_minute)
    print("Answer 1: {}".format(guard_id * sleepiest_minute))

    guard_id, minute, num_minutes = get_guard_with_most_single_minute(guards)
    print("Guard, minute, minutes of sleep:", guard_id, minute, num_minutes)

    print("Sleepiest guard * his sleepiest minute:", guard_id * minute)
    print("Answer 2: {}".format(guard_id * minute))


if __name__ == "__main__":
    main()
