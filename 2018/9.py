def add_marble(marbles):
    current_index = get_index_of_max(marbles)
    new_marble_no = len(marbles)
    if new_marble_no % 23 == 0:
        return new_marble_no
    new_index = (current_index + 1) % len(marbles) + 1
    print("antall marbles: {}, current index: {}, new index: {}".format(len(marbles), current_index, new_index))
    marbles.insert(new_index, new_marble_no)
    print("Marbles: {}".format(marbles))
    return 0


def get_index_of_max(marbles):
    max_value = 0
    index_of_max = 0
    for index in range(len(marbles)):
        if marbles[index] > max_value:
            max_value = marbles[index]
            index_of_max = index
    print("max index i {} er {}".format(marbles, index_of_max))
    return index_of_max


def main():
    # 430 players; last marble is worth 71588 points
    marbles = [0]
    # player_scores = [0] * 430
    player_scores = [0] * 9
    for player in range(1, len(player_scores)):
        print("legger til marble {}".format(player))
        player_scores[player] += add_marble(marbles)
        print("player {}, score {}".format(player, player_scores[player]))
    print("scores: {}".format(player_scores))
    print("Answer 1: {}".format(len(marbles)))
    # print("Answer 2: {}".format(sum2))


if __name__ == "__main__":
    main()
