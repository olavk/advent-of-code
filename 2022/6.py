#!/usr/bin/env python3
import os


def index_of_unique_seq(line: str, unique_len: int):
    for i in range(len(line)-unique_len):
        letters = line[i:i+unique_len]
        if len(set(letters)) == unique_len:
            return i + unique_len
        i += 1
    raise Exception


os.chdir(os.path.dirname(__file__))  # For å finne inputfila
with open('6-input.txt') as f:
    line = f.readline()

assert index_of_unique_seq('bvwbjplbgvbhsrlpgdmjqwftvncz', 4) == 5
assert index_of_unique_seq('nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg', 4) == 10
assert index_of_unique_seq('mjqjpqmgbljsphdztnvjfqwrcgsmlb', 14) == 19
assert index_of_unique_seq('nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg', 14) == 29

print("Part 1:", index_of_unique_seq(line, 4))
print("Part 2:", index_of_unique_seq(line, 14))
