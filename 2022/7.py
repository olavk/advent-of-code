#!/usr/bin/env python3
import os


class Node(object):
    """ Element iin file structure that can be a dir or a file """
    def __init__(self, name: str, parent: object = None, size: int = 0):
        self.name: str = name
        self.parent: object = parent
        self.size: int = size
        self.children: list[object] = []
        self.path: str = ""
        self.fullname: str = None
        if parent is None:
            self.path = ""
            self.fullname = "/"
        else:
            self.path = parent.fullname
            self.fullname = self.path.rstrip("/") + "/" + self.name

    def __str__(self):
        if self.size == 0:
            return f'{self.name}(DIR)'
        return f'{self.name}({self.size})'
        # return f'{self.name}({self.size}). Parent:{self.parent}, Children:{self.children}'

    def __repr__(self):
        return f'Node({self.path + self.name})'

    def sumsize(self):
        sum = 0
        for child in self.children:
            sum += child.sumsize()
        # print(f'Adding size of myself ({self.name}),{self.size}')
        sum += self.size
        # print(f'Sum children of {self.name}: {sum}')
        return sum


def make_fs() -> object:
    """ Returns struct of nodes based on inputfile """
    root = Node("/")
    current = root
    os.chdir(os.path.dirname(__file__))  # For å finne inputfila
    with open("7-input.txt") as f:
        for line in f:
            # print(f'Line:{line.strip()}')
            elems = line.split(" ")
            if elems[0] == "$":
                # print("Dette er en kommando")
                cmd = elems[1].strip()
                if cmd == "cd":
                    dirname = elems[2].strip()
                    # print(f'Bytt katalog fra {current.name} til {dirname}')
                    if dirname == "/":
                        current = root
                        continue
                    elif dirname == "..":
                        current = current.parent
                        # print(current)
                        continue
                    else:
                        # print(current)
                        for child in current.children:
                            if child.name == dirname:
                                current = child
                                continue
                        continue
                elif cmd == "ls":
                    # print('ls')
                    continue
            elif elems[0] == "dir":
                dirname = elems[1].strip()
                print(f'Adding dir {dirname} in {current.name}')
                current.children.append(Node(name=dirname, parent=current))
            elif elems[0].isnumeric():
                filename = elems[1].strip()
                filesize = int(elems[0])
                print(f'Adding file {filename} in {current.name} with size {filesize}')
                current.children.append(Node(name=filename, parent=current, size=filesize))
    return root


def size_of_children(current: object) -> int:
    """ Recursively computes size of all children of node """
    childsum = 0
    for child in current.children:
        if child.size > 0:
            childsum += child.size
        else:
            childsum += size_of_children(child)
    # print(f'Sum {current.name} and children: {childsum}')
    # childsums[current.name] = childsum
    return childsum


def treeprint(node: object, level: int = 0) -> None:
    """ Prints to screen an indented directory listing """
    if node.size == 0:
        print(f"{level * 4 * ' '}{node.fullname} (DIR). Total size: {size_of_children(node)}")
        for child in node.children:
            treeprint(child, level+1)
    else:
        print(f"{level * 4 * ' '}{node.name} ({node.size})")


def size_per_dir(node: object, dirlist = {}) -> dict[str, int]:
    """ Returns dict with fullname of dirs and sizes """
    if node.size == 0:
        dirlist[node.fullname] = size_of_children(node)
        for child in node.children:
            size_per_dir(child, dirlist)
    # else:
    #     dirlist[node.name] = node.size
    return dirlist


fs_root = make_fs()

print("\n\nPrint...")
treeprint(fs_root)

print("\n\nSizes...")
dirsizes = size_per_dir(fs_root)
print(dirsizes)

# Print sum of directories whose total size is above 1M
total_above_mill = 0
for s in dirsizes:
    if dirsizes[s] <= 100000:
        print(f"Adding {s}: {dirsizes[s]}")
        total_above_mill += dirsizes[s]
print("Total above mill:", total_above_mill)

# Your puzzle answer was 1325919.

# Find smallest dir that can be removed to have 30M free
fs_total = 70000000
fs_needed_free = 30000000
fs_used = size_of_children(fs_root)
fs_needed_freed = fs_needed_free - (fs_total - fs_used)
print(f'FS total: {fs_total}, Used:{fs_used}, Need to free:{fs_needed_freed}')

min_above_needed = fs_needed_free
for s in dirsizes:
    if dirsizes[s] > fs_needed_freed:
        min_above_needed = min(min_above_needed, dirsizes[s])

print("Size of dir to remove:", min_above_needed)
# Your puzzle answer was 2050735
