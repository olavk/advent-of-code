#!/usr/bin/env python3
""" Find overlapping assignments
18-20,19-21
9-86,9-87
"""
import os

os.chdir(os.path.dirname(__file__))  # For å finne inputfila
with open('4-input.txt') as f:
    assignments = f.readlines()

num_lines = 0
containing = 0
for line in assignments:
    num_lines += 1
    (f, t) = line.rstrip().split(",")
    (from1, to1) = [int(i) for i in f.split("-")]
    (from2, to2) = [int(i) for i in t.split("-")]
    if (from1 <= from2 and to1 >= to2) or (from2 <= from1 and to2 >= to1):
        # print(from1, to1, from2, to2)
        containing += 1

print(f'{containing} are fully containing, of {num_lines} total')

overlapping = 0
for line in assignments:
    num_lines += 1
    (f, t) = line.rstrip().split(",")
    (from1, to1) = [int(i) for i in f.split("-")]
    (from2, to2) = [int(i) for i in t.split("-")]
    if from1 <= to2 and to1 >= from2:
        print(from1, to1, from2, to2)
        overlapping += 1

print(f'{overlapping} are overlapping, of {num_lines} total')
