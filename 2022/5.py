#!/usr/bin/env python3
""" Move stacks of supply crates """
import os
import copy


def get_initial_state() -> list[str]:
    """ Return the lines that describe start state of crates """
    os.chdir(os.path.dirname(__file__))  # For å finne inputfila
    with open('5-input-start.txt') as f:
        lines = f.readlines()
    return lines


def get_moves() -> list[str]:
    """ Return move-lines read from file:
        move 2 from 2 to 7
        move 8 from 5 to 6
    """
    with open('5-input-moves.txt') as f:
        moves = f.readlines()
    return moves


def get_crate_struct(lines: str) -> list[list[str]]:
    """ Returns a list of 9 lists with crates, topmost last """
    crates = []
    for j in range(9):  # Hard coded 9 columns...
        crates.append([])
    for lineno in range(len(lines)-2,-1, -1):
        print(lines[lineno])
        for col in range(9):
            crate = lines[lineno][(col * 4) + 1]
            if crate != ' ':
                crates[col].append(crate)
    return crates


def apply_moves_9000(crate_struct, moves):
    """ Creates s copy of the crate-structure and applies the moves """
    crates = copy.deepcopy(crate_struct)
    for move in moves:
        words = move.split(' ')
        (qty, move_from, move_to) = [int(words[1]), int(words[3])-1, int(words[5])-1]
        # print(f"move {qty} from {crates[move_from]} to {crates[move_to]}")
        for i in range(qty):
            crates[move_to].append(crates[move_from].pop())
        # print(f"From now is {crates[move_from]}, to is {crates[move_to]}")
    return crates


def apply_moves_9001(crate_struct, moves):
    """ Creates s copy of the crate-structure and applies the moves """
    crates = copy.deepcopy(crate_struct)
    for move in moves:
        words = move.split(' ')
        (qty, move_from, move_to) = [int(words[1]), int(words[3])-1, int(words[5])-1]
        # print(f"Move {qty} from {crates[move_from]} to {crates[move_to]}")
        crates[move_to].extend(crates[move_from][-qty:])
        crates[move_from] = crates[move_from][:-qty]
        # print(f"From now is {crates[move_from]}, to is {crates[move_to]}")
    return crates


################
# Main program #
################

state_lines = get_initial_state()
crates = get_crate_struct(state_lines)
moves = get_moves()
print("At start", crates)
crates_after_9000 = apply_moves_9000(crates, moves)
print("After CrateMover 9000", crates_after_9000)

print("Topmost crates:", end="")
for crate in crates_after_9000:
    print(crate[-1], end="")
print()
# Your puzzle answer was JRVNHHCSJ.

# Now with CrateMover 9001
print("\nAt start", crates)
crates_after_9001 = apply_moves_9001(crates, moves)
print("After CrateMover 9001", crates_after_9001)
print("Topmost crates:", end="")
for crate in crates_after_9001:
    print(crate[-1], end="")
# Your puzzle answer was GNFBSBJLH.
