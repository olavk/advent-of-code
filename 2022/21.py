#!/usr/bin/env python3
import os


def get_input(filename) -> list[str]:
    """ Liste med linjene i fila """
    os.chdir(os.path.dirname(__file__))  # Inputfila er i samme katalog som scriptet
    lines = []
    with open(filename) as f:
        lines = f.readlines()
    return lines


def parse_input(lines) -> list:
    """ Liste med regnestykker """
    os.chdir(os.path.dirname(__file__))  # Inputfila er i samme katalog som scriptet
    jobs = {}
    for line in lines:
        # print(line)
        (monkey, job) = line.split(':')
        job = job.strip()
        if job.isnumeric():
            jobs[monkey] = int(job)
        else:
            # print(job.split(' '))
            (m1, operator, m2) = job.split(' ')
            jobs[monkey] = [m1, operator, m2]
    return jobs


test_jobs = [ "root: pppw + sjmn",
    "dbpl: 5",
    "cczh: sllz + lgvd",
    "zczc: 2",
    "ptdq: humn - dvpt",
    "dvpt: 3",
    "lfqf: 4",
    "humn: 5",
    "ljgn: 2",
    "sjmn: drzm * dbpl",
    "sllz: 4",
    "pppw: cczh / lfqf",
    "lgvd: ljgn * ptdq",
    "drzm: hmdt - zczc",
    "hmdt: 32"
]

input = get_input('21-input.txt')
# input = test_jobs
jobs = parse_input(input)

# print(jobs)

progress = True
while progress:
    progress = False
    for monkey, job in jobs.items():
        # print(monkey, job)
        if isinstance(jobs[monkey], int):
            continue
        else:
            m1 = jobs[monkey][0]
            m2 = jobs[monkey][2]
            operator = jobs[monkey][1]
            # print(m1, operator, m2)
            if isinstance(jobs[m1], int) and isinstance(jobs[m2], int):
                operation = f'{jobs[m1]} {operator if operator != "/" else "//"} {jobs[m2]}'
                jobs[monkey] = eval(operation)
            progress = True

# print(jobs)

print("Part 1:", jobs['root'])
assert jobs['root'] == 121868120894282
# Part 1: 121868120894282
