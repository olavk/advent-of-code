#!/usr/bin/env python3

import os


def priority(item: str):
    val = ord(item[0])
    if val > 96:
        pri = val - 96
    else:
        pri = val - 64 + 26
    # print(f'Pri of {item} is {pri}')
    return pri


def duplicate_letter(line1: str, line2: str):
    for c in line2:
        if c in line1:
            return c
    raise Exception


def dup_in_string(line: str):
    c1 = line[0:len(line) // 2]
    c2 = line[len(line) // 2:]
    # print(c1, c2)
    return duplicate_letter(c1, c2)


assert priority('p') == 16
assert priority('L') == 38
assert duplicate_letter('vJrwpWtwJgWr', 'hcsFMMfFFhFp') == 'p'
assert duplicate_letter('jqHRNqRjqzjGDLGL', 'rsFMfFZSrLrFZsSL') == 'L'
assert dup_in_string('vJrwpWtwJgWrhcsFMMfFFhFp') == 'p'
assert dup_in_string('jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL') == 'L'

os.chdir(os.path.dirname(__file__))  # For å finne inputfila
with open('3-input.txt') as f:
    sum = 0
    for line in f:
        dup = dup_in_string(line)
        sum += priority(dup)
        print(f'Dup:{dup}, pri:{priority(dup)}, line:{line}')
print(f'Sum: {sum}')
