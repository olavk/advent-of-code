#!/usr/bin/env python3
elfno = 1
elfsum = 0
elves = {}
with open('1-input.txt') as f:
    # input = f.readlines()
    for line in f:
        # print(line)
        if line.strip().isnumeric():
            elfsum += int(line.strip())
            # print('adding')
        else:
            elves[elfno] = elfsum
            elfno += 1
            elfsum = 0

elves_sorted = sorted(elves.items(), key=lambda x: x[1], reverse=True)

sum = 0
for i in range(0, 3):
    print(f'Alv {elves_sorted[i][0]}, kalorier: {elves_sorted[i][1]}')
    sum += elves_sorted[i][1]
print(f'Sum: {sum}')
