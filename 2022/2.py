#!/usr/bin/env python3
import os


itemscores = {'rock': 1, 'paper': 2, 'scissor': 3}
wins_over = {'rock': 'paper', 'paper': 'scissor', 'scissor': 'rock'}
loses_for = {'rock': 'scissor', 'paper': 'rock', 'scissor': 'paper'}
itemmap = {'A': 'rock', 'B': 'paper', 'C': 'scissor', 'X': 'rock', 'Y': 'paper', 'Z': 'scissor'}  # noqa: E501


def score(opponent: str, you: str):
    points = 0
    if opponent == you:
        points = 3
    elif wins_over[opponent] == you:
        points = 6
    points += itemscores[you]
    return points


assert score('rock', 'paper') == 6+2
assert score(itemmap['A'], itemmap['Y']) == 6+2
assert score('paper', 'rock') == 0+1
assert score(itemmap['Y'], itemmap['A']) == 0+1
assert score('scissor', 'scissor') == 3+3
assert score(itemmap['C'], itemmap['Z']) == 3+3


os.chdir(os.path.dirname(__file__))  # For å finne inputfila
with open('2-input.txt') as f:
    point_sum = 0
    for line in f:
        (opponent, you) = line.strip().split(' ')
        point_sum += score(itemmap[opponent], itemmap[you])

print(point_sum)

with open('2-input.txt') as f:
    point_sum = 0
    for line in f:
        (opponent, strategy) = line.strip().split(' ')
        opponent = itemmap[opponent]
        if strategy == 'X':
            you = loses_for[opponent]
        elif strategy == 'Y':
            you = opponent
        else:
            you = wins_over[opponent]
        point_sum += score(opponent, you)
        # print(f'opponent:{opponent}, strategy:{strategy}, you:{you}, sum:{point_sum}')  # noqa: E501

print(point_sum)
