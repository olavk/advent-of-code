.DEFAULT_GOAL := help
.PHONY: help test

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

test: ## Eksekverer hvert program, sjekker resultat mot forventet
	python3 2018/1.py | grep '^Answer 1: 439$$'
	python3 2018/1.py | grep '^Answer 2: 124645$$'
	python3 2018/2.py | grep '^Answer 1: 6696$$'
	python3 2018/2.py | grep '^Answer 2: bvnfawcnyoeyudzrpgslimtkj$$'
	python3 2018/3.py | grep '^Answer 1: 104241$$'
	python3 2018/3.py | grep '^Answer 2: 806$$'
	python3 2018/4.py | grep '^Answer 1: 151754$$'
	python3 2018/4.py | grep '^Answer 2: 19896$$'
	python3 2018/5.py | grep '^Answer 1: 10972$$'
	python3 2018/5.py | grep '^Answer 2: 5278$$'
	python3 2018/6.py | grep '^Answer 1: 3969$$'
	python3 2018/6.py | grep '^Answer 2: 42123$$'
	python3 2018/8.py | grep '^Answer 1: 40036$$'
	python3 2018/8.py | grep '^Answer 2: 21677$$'
