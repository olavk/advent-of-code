#!/usr/bin/env python3
import os
import logging

def get_input(day) -> list[str]:
    """ Liste med linjene i fila """
    os.chdir(os.path.dirname(__file__))  # Inputfila er i underkatalog av scriptet
    lines = []
    with open(f"inputs/{day}.txt") as f:
        content = f.readlines()
        for line in content:
            lines.append(line.strip())
    return lines

logging.basicConfig(level=logging.DEBUG)
