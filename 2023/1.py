#!/usr/bin/env python3
# import os
from tools import get_input, logging

DAY=1
digits = {"one":1, "two":2, "three":3, "four":4, "five":5, "six":6, "seven":7, "eight":8, "nine":9}


# ~~~~~~~~~~~~~~~ Functions ~~~~~~~~~~~~~~~ #

def get_numbers_in_line(line: str, include_words: bool = False) -> list[int]:
    """ Returner tallene (om de nå er tall eller ord) i linjen som en liste av ints """
    numbers_in_line = []
    for i, letter in enumerate(line):
        if letter.isdigit():
            numbers_in_line.append(int(letter))
            continue
        if include_words:
            for word, num in digits.items():
                if line[i:].startswith(word):
                    numbers_in_line.append(digits[word])
    logging.debug(f"numbers_in_line: {numbers_in_line}")
    return numbers_in_line


def sum_numbers_in_lines(input: list[str], include_words: bool = False) -> int:
    """ Returner summen av tallene i input """
    total = 0
    for line in input:
        logging.debug(f"line: {line}")
        nums = get_numbers_in_line(line, include_words)
        num = int((nums[0] * 10) + nums[-1])
        logging.debug(f"Number this line: {num}")
        total += num
    return total


# ~~~~~~~~~~~~~~~~ Testing ~~~~~~~~~~~~~~~~ #

testinput1=["1abc2", "pqr3stu8vwx", "a1b2c3d4e5f", "treb7uchet"]
assert sum_numbers_in_lines(testinput1, False) == 142

testinput2 = [
    "two1nine",
    "eightwothree",
    "abcone2threexyz",
    "xtwone3four",
    "4nineeightseven2",
    "zoneight234",
    "7pqrstsixteen",
]
assert sum_numbers_in_lines(testinput2, True) == 281


# ~~~~~~~~~~~~ Generate result ~~~~~~~~~~~~ #

data = get_input(DAY)


sol1 = sum_numbers_in_lines(data, False)
sol2 = sum_numbers_in_lines(data, True)
print(f"Løsning 1: {sol1}")
print(f"Løsning 2: {sol2}")
